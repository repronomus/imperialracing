
DROP FUNCTION IF EXISTS initcap;

DELIMITER $$

CREATE
    FUNCTION `initcap`(input VARCHAR(255))
    RETURNS VARCHAR(255)
    BEGIN
DECLARE len INT;
DECLARE i INT;

SET len   = CHAR_LENGTH(input);
SET input = LOWER(input);
SET i = 0;

WHILE (i < len) DO
	IF (MID(input,i,1) = ' ' OR i = 0) THEN
		IF (i < len) THEN
			SET input = CONCAT(
				LEFT(input,i),
				UPPER(MID(input,i + 1,1)),
				RIGHT(input,len - i - 1)
			);
		END IF;
	END IF;
	SET i = i + 1;
END WHILE;

RETURN input;
    END$$

DELIMITER ;

select initcap('cocodrilo');


DROP FUNCTION IF EXISTS limpia;

DELIMITER $$

CREATE FUNCTION limpia(str varchar(255))
  RETURNS varchar(255)
  BEGIN
  	set str=LOWER(str);
    set str=REPLACE(str,'@','');
    set str=REPLACE(str,'_',' ');
    set str=REPLACE(str,'-',' ');
    set str=REPLACE(str,'.',' ');
    set str=REPLACE(str,',',' ');
    set str=REPLACE(str,'(',' ');
    set str=REPLACE(str,')',' ');
    set str=REPLACE(str,'/',' ');
    set str=REPLACE(str,'1,',' ');
    set str=REPLACE(str,'2,',' ');
    set str=REPLACE(str,'3,',' ');
    set str=REPLACE(str,'4,',' ');
    set str=REPLACE(str,'5,',' ');
    set str=REPLACE(str,'6,',' ');
    set str=REPLACE(str,'7,',' ');
    set str=REPLACE(str,'8,',' ');
    set str=REPLACE(str,'9,',' ');
    set str=REPLACE(str,'0,',' ');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
    set str=REPLACE(str,'Á','A');
    set str=REPLACE(str,'É','E');
    set str=REPLACE(str,'Í','I');
    set str=REPLACE(str,'Ó','O');
    set str=REPLACE(str,'Ú','U');

    RETURN str;
  END;$$
  
DELIMITER ;
DROP FUNCTION IF EXISTS limpiaEmail;

DELIMITER $$

CREATE FUNCTION limpiaEmail(str varchar(255))
  RETURNS varchar(255)
  BEGIN
	set str=LOWER(str);
    set str=REPLACE(str,'(','');
    set str=REPLACE(str,')','');
    set str=REPLACE(str,'/','');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');

    RETURN str;
  END;$$
  
DELIMITER ;

DROP FUNCTION IF EXISTS limpiaPhones;


DELIMITER $$

CREATE FUNCTION limpiaPhones(str varchar(255))
  RETURNS varchar(255)
  BEGIN
	set str=LOWER(str);
	set str=REPLACE(str,' ','');
	set str=REPLACE(str,'@','');
	set str=REPLACE(str,',','');
	set str=REPLACE(str,'.','');
    set str=REPLACE(str,'(','');
    set str=REPLACE(str,')','');
    set str=REPLACE(str,'/','');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
	set str=REPLACE(str,'-','');
	set str=REPLACE(str,'_','');
	set str=REPLACE(str,'a','');
	set str=REPLACE(str,'b','');
	set str=REPLACE(str,'c','');
	set str=REPLACE(str,'d','');
	set str=REPLACE(str,'e','');
	set str=REPLACE(str,'f','');
	set str=REPLACE(str,'g','');
	set str=REPLACE(str,'h','');
	set str=REPLACE(str,'i','');
	set str=REPLACE(str,'j','');
	set str=REPLACE(str,'k','');
	set str=REPLACE(str,'l','');
	set str=REPLACE(str,'m','');
	set str=REPLACE(str,'n','');
	set str=REPLACE(str,'ñ','');
	set str=REPLACE(str,'o','');
	set str=REPLACE(str,'p','');
	set str=REPLACE(str,'q','');
	set str=REPLACE(str,'r','');
	set str=REPLACE(str,'s','');
	set str=REPLACE(str,'t','');
	set str=REPLACE(str,'u','');
	set str=REPLACE(str,'v','');
	set str=REPLACE(str,'w','');
	set str=REPLACE(str,'x','');
	set str=REPLACE(str,'y','');
	set str=REPLACE(str,'z','');


    RETURN str;
  END;$$
  
DELIMITER ;

SELECT ab.address_book_id as addressId,
initcap(ab.entry_firstname) as alias,
initcap(ab.entry_firstname)as firstname,
limpiaEmail(c.customers_email_address) as customerEmail,
initcap(ab.entry_company) as company,
initcap(ab.entry_lastname) as lastName,
initcap(ab.entry_street_address) as address,
ab.entry_postcode as postCode,
initcap(ab.entry_city) as city,
initcap(country.countries_name) as country,
initcap(limpiaPhones(c.customers_telephone)) as phone,
initcap(limpiaPhones(c.customers_fax)) as mobilePhone
FROM address_book ab
LEFT JOIN customers c ON c.customers_id = ab.customers_id 
LEFT JOIN countries country ON country.countries_id = ab.entry_country_id
WHERE country.countries_name = 'Spain'
ORDER BY c.customers_id ASC