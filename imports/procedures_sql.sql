
SET NAMES 'utf8';
DROP FUNCTION IF EXISTS limpia;

DELIMITER $$

CREATE FUNCTION limpiaStreet(str varchar(255))
  RETURNS varchar(255)
  BEGIN
  	set str=LOWER(str);
    set str=REPLACE(str, '&', ' ');
    set str=REPLACE(str, '·',' ');
    set str=REPLACE(str, 'ç', 'c');
    set str=REPLACE(str,'=', ' ');
    set str=REPLACE(str,'ï¿½', ' ');
    set str=REPLACE(str, '&#',' ');
    set str=REPLACE(str, 'Almu&#328ecar', 'Almuñecar');
	set str=REPLACE(str,'#', ' ');
	set str=REPLACE(str, "'", ' ');
    set str=REPLACE(str,'"','');
	set str=REPLACE(str,':','');
    set str=REPLACE(str,';','');
    set str=REPLACE(str,'@','');
    set str=REPLACE(str,'_',' ');
    set str=REPLACE(str,'-',' ');
    set str=REPLACE(str,'.',' ');
    set str=REPLACE(str,',',' ');
    set str=REPLACE(str,'(',' ');
    set str=REPLACE(str,')',' ');
    set str=REPLACE(str,'/',' ');
	set str=REPLACE(str,'\\',' ');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
    set str=REPLACE(str,'Á','A');
    set str=REPLACE(str,'É','E');
    set str=REPLACE(str,'Í','I');
    set str=REPLACE(str,'Ó','O');
    set str=REPLACE(str,'Ú','U');
	set str=REPLACE(str,'º',' ');
	set str=REPLACE(str,'ª',' ');
	set str=REPLACE(str,'ü','u');
	set str=REPLACE(str,'ë','e');
	set str=REPLACE(str,'à','a');
	set str=REPLACE(str,'è','e');
	set str=REPLACE(str,'ì','i');
	set str=REPLACE(str,'ò','o');
	set str=REPLACE(str,'ù','u');
	set str=REPLACE(str,'À','A');
	set str=REPLACE(str,'È','E');
	set str=REPLACE(str,'Ì','I');
	set str=REPLACE(str,'Ò','O');
	set str=REPLACE(str,'Ù','U');
    RETURN str;
  END;$$

DELIMITER ;

DROP FUNCTION IF EXISTS limpia;

DELIMITER $$

CREATE FUNCTION limpia(str varchar(255))
  RETURNS varchar(255)
  BEGIN
  	set str=LOWER(str);
    set str=REPLACE(str, '&', ' ');
    set str=REPLACE(str, '·',' ');
    set str=REPLACE(str, 'ç', 'c');
    set str=REPLACE(str,'=', ' ');
    set str=REPLACE(str,'ï¿½', ' ');
    set str=REPLACE(str, '&#',' ');
    set str=REPLACE(str, 'Almu&#328ecar', 'Almuñecar');
	set str=REPLACE(str,'#', ' ');
	set str=REPLACE(str, "'", ' ');
    set str=REPLACE(str,'\"','');
	set str=REPLACE(str,':','');
    set str=REPLACE(str,';','');
    set str=REPLACE(str,'@','');
    set str=REPLACE(str,'_',' ');
    set str=REPLACE(str,'-',' ');
    set str=REPLACE(str,'.',' ');
    set str=REPLACE(str,',',' ');
    set str=REPLACE(str,'(',' ');
    set str=REPLACE(str,')',' ');
    set str=REPLACE(str,'/',' ');
	set str=REPLACE(str,'\\',' ');
    set str=REPLACE(str,'1',' ');
    set str=REPLACE(str,'2',' ');
    set str=REPLACE(str,'3',' ');
    set str=REPLACE(str,'4',' ');
    set str=REPLACE(str,'5',' ');
    set str=REPLACE(str,'6',' ');
    set str=REPLACE(str,'7',' ');
    set str=REPLACE(str,'8',' ');
    set str=REPLACE(str,'9',' ');
    set str=REPLACE(str,'0',' ');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
    set str=REPLACE(str,'Á','A');
    set str=REPLACE(str,'É','E');
    set str=REPLACE(str,'Í','I');
    set str=REPLACE(str,'Ó','O');
    set str=REPLACE(str,'Ú','U');
	set str=REPLACE(str,'º',' ');
	set str=REPLACE(str,'ª',' ');
	set str=REPLACE(str,'ü','u');
	set str=REPLACE(str,'ë','e');
	set str=REPLACE(str,'à','a');
	set str=REPLACE(str,'è','e');
	set str=REPLACE(str,'ì','i');
	set str=REPLACE(str,'ò','o');
	set str=REPLACE(str,'ù','u');
	set str=REPLACE(str,'À','A');
	set str=REPLACE(str,'È','E');
	set str=REPLACE(str,'Ì','I');
	set str=REPLACE(str,'Ò','O');
	set str=REPLACE(str,'Ù','U');
    RETURN str;
  END;$$

DELIMITER ;
DROP FUNCTION IF EXISTS limpiaEmail;

DELIMITER $$

CREATE FUNCTION limpiaEmail(str varchar(255))
  RETURNS varchar(255)
  BEGIN
	set str=LOWER(str);
	set str=REPLACE(str,'\"','');
	set str=REPLACE(str,':','');
    set str=REPLACE(str,';','');
    set str=REPLACE(str,'(','');
    set str=REPLACE(str,')','');
    set str=REPLACE(str,'/','');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
    set str=REPLACE(str, '&', ' ');
    set str=REPLACE(str, '·',' ');
    set str=REPLACE(str, 'ç', 'c');
    set str=REPLACE(str,'=', ' ');
    set str=REPLACE(str,'ï¿½', ' ');
    set str=REPLACE(str, '&#',' ');

    RETURN str;
  END;$$

DELIMITER ;

DROP FUNCTION IF EXISTS limpiaPhones;


DELIMITER $$

CREATE FUNCTION limpiaPhones(str varchar(255))
  RETURNS varchar(255)
  BEGIN
	set str=LOWER(str);
	set str=REPLACE(str,'\"','');
	set str=REPLACE(str,':','');
    set str=REPLACE(str,';','');
	set str=REPLACE(str,' ','');
	set str=REPLACE(str,'@','');
	set str=REPLACE(str,',','');
	set str=REPLACE(str,'.','');
    set str=REPLACE(str,'(','');
    set str=REPLACE(str,')','');
    set str=REPLACE(str,'/','');
    set str=REPLACE(str,'á','a');
    set str=REPLACE(str,'é','e');
    set str=REPLACE(str,'í','i');
    set str=REPLACE(str,'ó','o');
    set str=REPLACE(str,'ú','u');
	set str=REPLACE(str,'-','');
	set str=REPLACE(str,'_','');
	set str=REPLACE(str,'a','');
	set str=REPLACE(str,'b','');
	set str=REPLACE(str,'c','');
	set str=REPLACE(str,'d','');
	set str=REPLACE(str,'e','');
	set str=REPLACE(str,'f','');
	set str=REPLACE(str,'g','');
	set str=REPLACE(str,'h','');
	set str=REPLACE(str,'i','');
	set str=REPLACE(str,'j','');
	set str=REPLACE(str,'k','');
	set str=REPLACE(str,'l','');
	set str=REPLACE(str,'m','');
	set str=REPLACE(str,'n','');
	set str=REPLACE(str,'ñ','');
	set str=REPLACE(str,'o','');
	set str=REPLACE(str,'p','');
	set str=REPLACE(str,'q','');
	set str=REPLACE(str,'r','');
	set str=REPLACE(str,'s','');
	set str=REPLACE(str,'t','');
	set str=REPLACE(str,'u','');
	set str=REPLACE(str,'v','');
	set str=REPLACE(str,'w','');
	set str=REPLACE(str,'x','');
	set str=REPLACE(str,'y','');
	set str=REPLACE(str,'z','');
	set str=REPLACE(str, '&', ' ');
    set str=REPLACE(str, '·',' ');
    set str=REPLACE(str, 'ç', 'c');
    set str=REPLACE(str,'=', ' ');
    set str=REPLACE(str,'ï¿½', ' ');
    set str=REPLACE(str, '&#',' ');

    RETURN str;
  END;$$

DELIMITER ;

DROP FUNCTION IF EXISTS initcap;

DELIMITER $$

CREATE
    FUNCTION `initcap`(input VARCHAR(255))
    RETURNS VARCHAR(255)
    BEGIN
DECLARE len INT;
DECLARE i INT;

SET len   = CHAR_LENGTH(input);
SET input = LOWER(input);
SET i = 0;

WHILE (i < len) DO
	IF (MID(input,i,1) = ' ' OR i = 0) THEN
		IF (i < len) THEN
			SET input = CONCAT(
				LEFT(input,i),
				UPPER(MID(input,i + 1,1)),
				RIGHT(input,len - i - 1)
			);
		END IF;
	END IF;
	SET i = i + 1;
END WHILE;

RETURN input;
    END$$

DELIMITER ;

#Direcciones

SELECT ab.address_book_id as addressId,
initcap(SUBSTRING(limpia(ab.entry_firstname), 1, 255)) as alias,
initcap(SUBSTRING(limpia(ab.entry_firstname),1, 255))as firstname,
limpiaEmail(c.customers_email_address) as customerEmail,
initcap(limpia(ab.entry_company)) as company,
initcap(limpia(ab.entry_lastname)) as lastName,
initcap(SUBSTRING(limpiaStreet(ab.entry_street_address),1 ,127)) as address,
limpiaPhones(ab.entry_postcode) as postCode,
initcap(limpia(ab.entry_city)) as city,
initcap(limpia(country.countries_name)) as country,
initcap(limpiaPhones(c.customers_telephone)) as phone,
initcap(limpiaPhones(c.customers_fax)) as mobilePhone
FROM address_book ab
LEFT JOIN customers c ON c.customers_id = ab.customers_id
LEFT JOIN countries country ON country.countries_id = ab.entry_country_id
WHERE country.countries_name = 'Spain'
ORDER BY c.customers_id ASC